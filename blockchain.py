# Module 1 - Create a Blockchain
"""
Created on Sun Jun 23 19:23:28 2019

@author: Sophia Price
"""

# Importing the libraries
import datetime
import hashlib
import json
from flask import Flask, jsonify

# Part 1 - Building a Blockchain
class Blockchain:

    def __init__(self):
        # Create the chain
        self.chain = []
        # Create the genesis block
        self.create_block(proof = 1, previous_hash = '0')

    def create_block(self, proof, previous_hash):
        block = {
                 'index': len(self.chain) + 1,
                 'timestamp': str(datetime.datetime.now()),
                 'proof': proof,
                 'previous_hash': previous_hash
                }
        # Append block to the chain
        self.chain.append(block)

        return block

    # Get the last block of the current chain
    def get_previous_block(self):
        return self.chain[-1]

    # Proof of work for mining
    def proof_of_work(self, previous_proof):
        new_proof = 1
        check_proof = False
        while check_proof is False:
            # More 0's => Harder to mine
            hash_operation = hashlib.sha256(str(new_proof**2 - previous_proof**2).encode()).hexdigest()
            if hash_operation[:4] == '0000':
                check_proof = True
            else:
                new_proof += 1
        return new_proof

    # Get hash block
    def hash(self, block):
        encoded_block = json.dumps(block, sort_keys = True).encode()
        return hashlib.sha256(encoded_block).hexdigest()

    # Check chain valid
    def is_chain_valid(self, chain):
        previous_block = chain[0]
        block_index = 1
        while block_index < len(chain):
            block = chain[block_index]
            # Check previous_hash = hash of previous block
            if block['previous_hash'] != self.hash(previous_block):
                # Chain not valid
                return False
            # Check proof of each block is valid
            previous_proof = previous_block['proof']
            proof = block['proof']
            hash_operation = hashlib.sha256(str(proof**2 - previous_proof**2).encode()).hexdigest()
            if hash_operation[:4] != '0000':
                return False
            # Update block & loop var
            previous_block = block
            block_index += 1
        # Chain is valid
        return True

# Part 2 - Mining our Blockchain
# Creating a web app
app = Flask(__name__)

# Creating a blockchain
blockchain = Blockchain()

# Mining a new block
@app.route('/mine_block')
